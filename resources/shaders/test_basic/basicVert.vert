#version 410 core

layout (location = 0) in vec4 position;
layout (location = 1) in vec4 color;


uniform mat4 worldMatrix;
uniform mat4 rotation ;
uniform float angle;
uniform mat4 quat;
uniform mat4 quatC;

out vec4 glColor;




void main(){

  vec4 testPosition = vec4(position.xyz,1);
  vec4 rotatedPosition = rotation * testPosition;
  gl_Position = worldMatrix * rotatedPosition ;

  glColor = color;
}
