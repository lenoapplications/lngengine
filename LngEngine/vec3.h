#pragma once
#include "vec_model.h"


template<typename type>
class Vec3 : public VecX<type, 3>{


public:
	Vec3();
	~Vec3();


	type* x;
	type* y;
	type* z;

private:
protected:
};

template <typename type>
Vec3<type>::Vec3()
{
	x = &this->vec[0];
	y = &this->vec[1];
	z = &this->vec[2];
}

template <typename type>
Vec3<type>::~Vec3()
{
}




