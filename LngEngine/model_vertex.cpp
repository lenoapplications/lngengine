#include "model_vertex.h"

ModelVertex::ModelVertex()
{
	
	this->position = glm::vec3(0, 0, -5);
	this->rotation = glm::vec3(0, 0, 0);
}

ModelVertex::~ModelVertex()
{
}

glm::vec3* ModelVertex::getRotation()
{
	return &this->rotation;
}

glm::vec3* ModelVertex::getPosition()
{
	return &this->position;
}

glm::vec3* ModelVertex::getScale()
{
	return &this->scale;
}

void ModelVertex::increaseXPosition(float x)
{
	this->position.x += x;
}

void ModelVertex::increaeYPosition(float y)
{
	this->position.y += y;
}

void ModelVertex::increaseZPosition(float z)
{
	this->position.z += z;
}

void ModelVertex::increaseXAngle(float x)
{
	this->rotation.x += x;
}

void ModelVertex::increaseYAngle(float y)
{
	this->rotation.y += y;
}

void ModelVertex::increaseZAngle(float z)
{
	this->rotation.z += z;
}

void ModelVertex::setXAngle(float x)
{
	this->rotation.x = x;
}

void ModelVertex::setYAngle(float y)
{
	this->rotation.y = y;
}

void ModelVertex::setZAngle(float z)
{
	this->rotation.z = z;
}
