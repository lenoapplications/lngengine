#pragma once
#include "mat_model.h"


template<typename type>
class Mat4x4 : public MatXxX<type, 4, 4> {

public:
    Mat4x4();
    ~Mat4x4() override;

    void print() {
        for (int i = 0; i < this->rowSize; ++i) {
            for (int j = 0; j < this->columnSize; ++j) {
                std::cout << this->matrix[i][j] << "  | ";
            };
            std::cout << "\n" << std::endl;
        };
    }


private:

protected:

};

/*/
 * Constructor
 */
template<typename type>
Mat4x4<type>::Mat4x4() {

}

/*
 * Destructor
 */
template<typename type>
Mat4x4<type>::~Mat4x4() {

}

