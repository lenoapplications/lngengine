#include "development_def.h"

void messageCallback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message,
	const void* userParam)
{
	std::cout << "GL_CALLBACK : " << "dataTarget : " << type << "\n" << "severity : " << severity << "\n" << "message : " << message << std::endl;

}
