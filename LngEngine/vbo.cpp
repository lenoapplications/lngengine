#include "vbo.h"

Vbo::~Vbo()
{
}

void Vbo::setBasicDescription(int attrIndex,int target,int typeOfUsage,int dataType,int sizeOfDataType,int sizeOfVertex)
{
	this->vboDescriptor.target = target;
	this->vboDescriptor.typeOfUsage = typeOfUsage;
	this->vboDescriptor.attrIndex = attrIndex;
	this->vboDescriptor.dataType = dataType;
	this->vboDescriptor.sizeOfVertex = sizeOfVertex;
	this->vboDescriptor.sizeOfDataType = sizeOfDataType;
}

void Vbo::createVbo(int numberToGenerate)
{
	glGenBuffers(numberToGenerate, &vboDescriptor.id);
	std::cout << &vboDescriptor.id << std::endl;
}

void Vbo::bindVbo()
{
	glBindBuffer(vboDescriptor.target, vboDescriptor.id);
	std::cout << "binding vbo " << vboDescriptor.id << std::endl;
}

void Vbo::fillVbo(void* data, int sizeOfData)
{
	vboDescriptor.sizeOfData = sizeOfData;
	glBufferData(vboDescriptor.target, sizeOfData,data,vboDescriptor.typeOfUsage);
	
	
}

void Vbo::setLayoutOfData()
{
	glVertexAttribPointer(vboDescriptor.attrIndex, 
		vboDescriptor.sizeOfVertex, 
		vboDescriptor.dataType, 
		GL_FALSE, 
		this->vboDescriptor.sizeOfDataType * this->vboDescriptor.sizeOfVertex
		,0);
}
