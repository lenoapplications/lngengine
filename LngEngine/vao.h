#pragma once
#include "glDescriptor.h"

class Vao
{

public:
	Vao() = default;
	~Vao();

	void createVao(int numberOfVaoToGenerate);
	void activateVao();
private:
	VaoDescriptor vaoDescriptor;
protected:
};

