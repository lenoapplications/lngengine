#pragma once



template<class TYPE, int VEC_SIZE>
class VecX{

public:
	VecX();
	virtual ~VecX();
	
private:
	
protected:
	TYPE vec[VEC_SIZE];
};

template <class TYPE, int VEC_SIZE>
VecX<TYPE, VEC_SIZE>::VecX()
{
}

template <class TYPE, int VEC_SIZE>
VecX<TYPE, VEC_SIZE>::~VecX()
{
}

