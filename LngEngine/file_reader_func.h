#pragma once
#include <fstream>
#include <string>
#include <iostream>

namespace fileReaderFunc
{

	void readShaderFile(std::string pathToFile, std::string* srcCode);
}

