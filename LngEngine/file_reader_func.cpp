#include "file_reader_func.h"



void fileReaderFunc::readShaderFile(std::string pathToFile, std::string* srcCode)
{
	std::ifstream file(pathToFile.c_str());
	std::string line;

	while(std::getline(file,line))
	{
		srcCode->append(line);
		srcCode->append("\n");
	}
	
}
