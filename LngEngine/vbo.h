#pragma once
#include "glDescriptor.h"

class Vbo
{
public:
	Vbo()=default;
	~Vbo();

	void setBasicDescription(int attrIndex,int target,int typeOfUsage,int dataType,int sizeOfDataType,int sizeOfVertex);
	void createVbo(int numberToGenerate);
	void bindVbo();
	void fillVbo(void* data,int sizeOfData);
	void setLayoutOfData();
	
protected:
private:
	VboDescriptor vboDescriptor;
};

