#include "model_matrix.h"

#include <iostream>

ModelMatrix::ModelMatrix()
{
	this->perspective = glm::mat4(1.0f);
}
ModelMatrix::~ModelMatrix()
{
	
}



void ModelMatrix::setPerspectiveMatrix(float fov, float near, float far, float aspect)
{
	this->perspective = glm::perspective(fov, aspect, near, far);
}

glm::mat4 ModelMatrix::getComputedMatrix(ModelVertex* modelVertex)
{

	glm::vec3* rotationAngles = modelVertex->getRotation();

	glm::mat4 translate = glm::translate(glm::mat4(1.0f), *modelVertex->getPosition());

	
	return this->perspective *translate;
}

glm::mat4 ModelMatrix::getRotation()
{
	angle += 0.01;
	float result = angle / 2;
	float x = 0.79 * sin(result);
	float y = 0.42 * sin(result);
	float z = 0.44 * sin(result);
	float w = cos(result);

	glm::quat q = glm::quat(w, x, y, z);

	return glm::toMat4(q);
}

glm::mat4 ModelMatrix::getRotationConjugate()
{
	glm::quat quat;

	quat.w = std::cos(1.57079633);
	quat.x = std::sin(1.57079633) * 0.0f;
	quat.y = std::sin(1.57079633) * 0.77f;
	quat.z = std::sin(1.57079633) * 0.64f;

	glm::quat cquat = glm::conjugate(quat);
	
	return glm::mat4_cast(cquat);
	
}


void ModelMatrix::printMatrices()
{
	std::cout << glm::to_string( perspective);
}
