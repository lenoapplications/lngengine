#pragma once
#include "vec3.h"

/***
 * Abstract class that is inherited by all the matrices
 */
template<class TYPE, int ROW_SIZE, int COLUMN_SIZE>
class MatXxX {
	
public:
    MatXxX();
    virtual ~MatXxX();


    TYPE* getPointerToMatrix();

	
    /***
     * @tparam MATRIX -> perspective dataTarget. Matrix 4 by 4 for example
     * @param matXxX  -> pointer to perspective
     *
     * friend function (allowed to access private part of MatXxX) that sets passed perspective to identity [1 0 0 0]
     *                                                                                                [0 1 0 0]
     *                                                                                                [0 0 1 0]
     *                                                                                                [0 0 0 1]
     */
    template<typename MATRIX>
    friend void setIdentity(MATRIX* matXxX);

    /***
     *
     * @tparam MATRIX -> perspective dataTarget. Matrix 4 by 4 for example
     * @tparam NUMBER_TYPE x,y,x->dataTarget of primitive,float,double,integer...
     * @param matXxX -> pointer to perspective
     *
     */
    template<typename MATRIX, typename NUMBER_TYPE>
    friend void set3dTranslationMatrix(MATRIX* matXxX, NUMBER_TYPE x, NUMBER_TYPE y, NUMBER_TYPE z);


    template<typename MATRIX, typename NUMBER_TYPE>
    friend void set3dRotationMatrix(MATRIX* matXxX, int x, int y, int z, Vec3<NUMBER_TYPE> angle);


    template<typename MATRIX, typename NUMBER_TYPE>
    friend void setRotationMatrixXFor3d(MATRIX* matXxX, NUMBER_TYPE angle);


    template<typename MATRIX, typename NUMBER_TYPE>
    friend void setRotationMatrixYFor3d(MATRIX* matXxX, NUMBER_TYPE angle);


    template<typename MATRIX, typename NUMBER_TYPE>
    friend void setRotationMatrixZFor3d(MATRIX* matXxX, NUMBER_TYPE angle);


    template<typename MATRIX, typename NUMBER_TYPE>
    friend void setPerspectiveMatrix(MATRIX* matXxX, NUMBER_TYPE filedOfView,NUMBER_TYPE nearPlane,NUMBER_TYPE farPlane,NUMBER_TYPE aspectRatio);

	

private:
protected:
    TYPE matrix[ROW_SIZE][COLUMN_SIZE];
    const int rowSize = ROW_SIZE;
    const int columnSize = COLUMN_SIZE;

    /***
     * function that initialize perspective to 0
     */
    void initialize();
};

/*/
 * Constructor
 */
template<class TYPE, int ROW_SIZE, int COLUMN_SIZE>
MatXxX<TYPE, ROW_SIZE, COLUMN_SIZE>::MatXxX() {
    initialize();
}

/*
 * Destructor
 */
template<class TYPE, int ROW_SIZE, int COLUMN_SIZE>
MatXxX<TYPE, ROW_SIZE, COLUMN_SIZE>::~MatXxX() {

}



//Class functions

template<class TYPE, int ROW_SIZE, int COLUMN_SIZE>
void MatXxX<TYPE, ROW_SIZE, COLUMN_SIZE>::initialize() {
    for (int i = 0; i < this->rowSize; ++i) {
        for (int j = 0; j < this->columnSize; ++j) {
        
            this->matrix[i][j] = 0.;
        };
    };
}
template <class TYPE, int ROW_SIZE, int COLUMN_SIZE>
TYPE* MatXxX<TYPE, ROW_SIZE, COLUMN_SIZE>::getPointerToMatrix()
{
    return &matrix[0][0];
}







//Friend functions for matrices

template<typename MATRIX>
void setIdentity(MATRIX* matXxX) {
    for (int i = 0, j = 0; i < matXxX->rowSize; ++i, ++j) {
        matXxX->matrix[i][j] = 1.;
    }
}

template<typename MATRIX, typename NUMBER_TYPE>
void set3dTranslationMatrix(MATRIX* matXxX, NUMBER_TYPE x, NUMBER_TYPE y, NUMBER_TYPE z) {
    matXxX->matrix[3][0] = x;   
    matXxX->matrix[3][1] = y;
    matXxX->matrix[3][2] = z;
}



template<typename MATRIX, typename NUMBER_TYPE>
void set3dRotationMatrix(MATRIX* matXxX, int x, int y, int z, Vec3<NUMBER_TYPE> angle)
{
    NUMBER_TYPE castedX = (NUMBER_TYPE)x;
    NUMBER_TYPE castedY = (NUMBER_TYPE)y;
    NUMBER_TYPE castedZ = (NUMBER_TYPE)z;
	
    NUMBER_TYPE xA = *angle.x;
    NUMBER_TYPE yA = *angle.y;
    NUMBER_TYPE zA = *angle.z;

    NUMBER_TYPE cosX = cos(xA);
    NUMBER_TYPE sinX = sin(xA);
    NUMBER_TYPE NsinX = -sinX;

    NUMBER_TYPE cosY = cos(yA);
    NUMBER_TYPE sinY = sin(yA);
    NUMBER_TYPE NsinY = -sinY;

    NUMBER_TYPE cosZ = cos(zA);
    NUMBER_TYPE sinZ = sin(zA);
    NUMBER_TYPE NsinZ = -sinZ;
	
    matXxX->matrix[1][1] = (castedX * cosX) + (castedZ * cosZ);
    matXxX->matrix[2][1] = castedX * NsinX;
    matXxX->matrix[1][2] = castedX * sinX;
    matXxX->matrix[2][2] = (castedX * cosX) + (castedY * cosY);

	
    matXxX->matrix[0][0] = (castedZ * cosZ) + (castedY * cosY);
    matXxX->matrix[1][0] = (castedZ * NsinZ);
    matXxX->matrix[0][1] = (castedZ * sinZ);

	
    matXxX->matrix[2][0] = (castedY * sinY);
    matXxX->matrix[0][2] = (castedY * NsinY);

	

	
}

template<typename MATRIX, typename NUMBER_TYPE>
void setRotationMatrixXFor3d(MATRIX* matXxX, NUMBER_TYPE angle) {

    matXxX->matrix[1][1] = cos(angle);
    matXxX->matrix[2][1] = -sin(angle);
    matXxX->matrix[1][2] = sin(angle);
    matXxX->matrix[2][2] = cos(angle);

}

template<typename MATRIX, typename NUMBER_TYPE>
void setRotationMatrixZFor3d(MATRIX* matXxX, NUMBER_TYPE angle) {
	
    matXxX->matrix[0][0] = cos(angle);
    matXxX->matrix[1][0] = -sin(angle);
    matXxX->matrix[0][1] = sin(angle);
    matXxX->matrix[1][1] = cos(angle);
}

template<typename MATRIX, typename NUMBER_TYPE>
void setRotationMatrixYFor3d(MATRIX* matXxX, NUMBER_TYPE angle) {
	
    matXxX->matrix[0][0] = cos(angle);
    matXxX->matrix[2][0] = sin(angle);
    matXxX->matrix[0][2] = -sin(angle);
    matXxX->matrix[2][2] = cos(angle);
}



template<typename MATRIX, typename NUMBER_TYPE>
void setPerspectiveMatrix(MATRIX* matXxX, NUMBER_TYPE fieldOfView, NUMBER_TYPE nearPlane, NUMBER_TYPE farPlane, NUMBER_TYPE aspectRatio)
{
    NUMBER_TYPE halfAngle = fieldOfView / (NUMBER_TYPE)2;
    matXxX->matrix[0][0] = 1 / (tan(halfAngle));
    matXxX->matrix[1][1] = 1 / (tan(halfAngle));
    matXxX->matrix[2][2] = -(farPlane + nearPlane) / (farPlane - nearPlane);
    matXxX->matrix[3][2] = -(2 * farPlane * nearPlane) / (farPlane - nearPlane);
    matXxX->matrix[2][3] = -1;
}

