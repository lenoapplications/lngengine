#pragma once
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <iostream>

#define DEVELOPMENT

#define OPENGL_VERSION 4


#ifdef DEVELOPMENT

	#if OPENGL_VERSION == 4
		void GLAPIENTRY messageCallback(GLenum source,
		    GLenum type,
		    GLuint id,
		    GLenum severity,
		    GLsizei length,
		    const GLchar* message,
		    const void* userParam);
	#endif



#endif
