#include "quaternion.h"


Quaternion::Quaternion(float x, float y, float z, float w):x(x),y(y),z(z),w(w)
{
	
}

Quaternion::~Quaternion()
{
}

float Quaternion::length()
{
	
	return sqrt((x * x) + (y * y) + (z * z) + (w * w));
}

Quaternion* Quaternion::normalize()
{
	float vectorLength = length();

	this->x /= vectorLength;
	this->y /= vectorLength;
	this->z /= vectorLength;
	this->w /= vectorLength;
	
	return this;

	
}

std::shared_ptr<Quaternion> Quaternion::conjugate()
{
	return std::make_shared<Quaternion>(-x, -y, -z, w);
}

std::shared_ptr<Quaternion> Quaternion::mul(Quaternion* quaternion)
{
	float w_ = (w * quaternion->w) - (x * quaternion->x) - (y * quaternion->y) - (z * quaternion->z);
	float x_ = (x * quaternion->w) + (w * quaternion->x) + (y * quaternion->z) - (z * quaternion->y);
	float y_ = (y * quaternion->w) + (w * quaternion->y) + (z * quaternion->x) - (x * quaternion->z);
	float z_ = (z * quaternion->w) + (w * quaternion->z) + (x * quaternion->y) - (y * quaternion->x);
	
	return std::make_shared<Quaternion>(x_, y_, z_, w_);
}
