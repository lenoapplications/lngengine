#pragma once
#include <iostream>
#include<cmath>

class Quaternion
{
public:
	Quaternion()=default;
	Quaternion(float x,float y,float z,float w);
	~Quaternion();


	float length();
	Quaternion* normalize();
	std::shared_ptr<Quaternion> conjugate();
	std::shared_ptr<Quaternion> mul(Quaternion* quaternion);
private:
protected:
	float x;
	float y;
	float z;
	float w;
};

