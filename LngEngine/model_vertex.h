#pragma once
#include "glm.hpp"
#include "gtc/matrix_transform.hpp"
#include "gtx/string_cast.hpp"

class ModelVertex
{

public:
	ModelVertex();
	~ModelVertex();

	glm::vec3* getRotation();
	glm::vec3* getPosition();
	glm::vec3* getScale();



	void increaseXPosition(float x);
	void increaeYPosition(float y);
	void increaseZPosition(float z);

	void increaseXAngle(float x);
	void increaseYAngle(float y);
	void increaseZAngle(float z);

	void setXAngle(float x);
	void setYAngle(float y);
	void setZAngle(float z);
	
private:
	glm::vec3 rotation;
	glm::vec3 position;
	glm::vec3 scale;
protected:
};

