#include "vao.h"

Vao::~Vao()
{
}

void Vao::createVao(int numberOfVaoToGenerate)
{
	glGenVertexArrays(numberOfVaoToGenerate, &vaoDescriptor.id);

}

void Vao::activateVao()
{
	glBindVertexArray(vaoDescriptor.id);
}
