// LngEngine.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include "development_def.h"
#include "vao.h"
#include "vbo.h"
#include "file_reader_func.h"
#include "mat_4x4.h"
#include "glm.hpp"
#include "gtc/matrix_transform.hpp"
#include "gtx/string_cast.hpp"
#include "model_matrix.h"
#include "gtx/quaternion.hpp"
#include "gtc/quaternion.hpp"
#include <ctime>
#include <thread>


using namespace fileReaderFunc;



static unsigned int CompileShader(const std::string& shader,unsigned int type)
{
    unsigned int vs = glCreateShader(type);
    const char* src = shader.c_str();
    glShaderSource(vs, 1, &src, nullptr);
    glCompileShader(vs);

    int result;
    glGetShaderiv(vs, GL_COMPILE_STATUS, &result);

	if(result == GL_FALSE)
	{
        int length;
        glGetShaderiv(vs, GL_INFO_LOG_LENGTH, &length);
		
        char* message = (char*)alloca(length * sizeof(char));

        glGetShaderInfoLog(vs, length, &length, message);

        std::cout << "message " << message << std::endl;
	}
    return vs;
}

static unsigned int CreateShader(const std::string& vertexShader,const std::string& fragmentShader)
{
    unsigned int program = glCreateProgram();
    unsigned int vs = CompileShader(vertexShader, GL_VERTEX_SHADER);
    unsigned int fs = CompileShader(fragmentShader, GL_FRAGMENT_SHADER);

    glAttachShader(program, vs);
    glAttachShader(program, fs);

    glLinkProgram(program);
    glValidateProgram(program);

    glDeleteShader(vs);
    glDeleteShader(fs);

    return program;
}


int main()
{
    std::cout << "Hello World!\n";
    GLFWwindow* window;
    if (!glfwInit())
    {
        throw std::exception("Need to build system for this");
    }
    else {
        int width = 1024;
        int height = 720;

        glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE);

        window = glfwCreateWindow(width, height, (char*)"title", NULL, NULL);

        if (!window) {
            glfwTerminate();
            throw std::exception("Need to build system for this");
        }

        float x = 0.0f;
        glfwMakeContextCurrent(window);

        if (glewInit() != GLEW_OK) {
            glfwTerminate();
            throw std::exception("Need to build system for this");
        }

        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
        glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);


        glEnable(GL_DEBUG_OUTPUT);
 
        glDebugMessageCallback(messageCallback, nullptr);


        float positions[72] = {
				-0.5f,0.5f,-0.5f,
                -0.5f,-0.5f,-0.5f,
                0.5f,-0.5f,-0.5f,
                0.5f,0.5f,-0.5f,

                -0.5f,0.5f,0.5f,
                -0.5f,-0.5f,0.5f,
                0.5f,-0.5f,0.5f,
                0.5f,0.5f,0.5f,

                0.5f,0.5f,-0.5f,
                0.5f,-0.5f,-0.5f,
                0.5f,-0.5f,0.5f,
                0.5f,0.5f,0.5f,

                -0.5f,0.5f,-0.5f,
                -0.5f,-0.5f,-0.5f,
                -0.5f,-0.5f,0.5f,
                -0.5f,0.5f,0.5f,

                -0.5f,0.5f,0.5f,
                -0.5f,0.5f,-0.5f,
                0.5f,0.5f,-0.5f,
                0.5f,0.5f,0.5f,

                -0.5f,-0.5f,0.5f,
                -0.5f,-0.5f,-0.5f,
                0.5f,-0.5f,-0.5f,
                0.5f,-0.5f,0.5f
        };

        float colors[72] = {
             0.0f,0.0f,1.8f,
             0.0f,1.0f,0.0f,
             0.0f,0.83f,1.0f,
			 1.0f,0.0f,0.0f,

             0.0f,0.0f,1.8f,
             0.0f,1.0f,0.0f,
             0.0f,0.83f,1.0f,
             1.0f,0.0f,0.0f,

             0.0f,0.0f,1.8f,
             0.0f,1.0f,0.0f,
             0.0f,0.83f,1.0f,
             1.0f,0.0f,0.0f,

             0.0f,0.0f,1.8f,
             1.0f,1.0f,0.0f,
             1.0f,0.83f,1.0f,
             1.0f,0.0f,0.0f,

             1.0f,0.0f,1.8f,
             1.0f,1.0f,0.0f,
             1.0f,0.83f,1.0f,
             1.0f,0.0f,0.0f,

             0.0f,0.0f,1.8f,
             0.0f,1.0f,0.0f,
             0.0f,0.83f,1.0f,
             1.0f,0.0f,0.0f,
        };

        int index[36] = {
                0,1,3,
                3,1,2,
                4,5,7,
                7,5,6,
                8,9,11,
                11,9,10,
                12,13,15,
                15,13,14,
                16,17,19,
                19,17,18,
                20,21,23,
                23,21,22

        };

        Vao vao;

        vao.createVao(1);
        vao.activateVao();

        Vbo vbo;
        vbo.createVbo(1);
        vbo.setBasicDescription(0, GL_ARRAY_BUFFER, GL_STATIC_DRAW, GL_FLOAT, sizeof(float), 3);
        vbo.bindVbo();
        vbo.fillVbo(positions, sizeof(float) * 72);
    	vbo.setLayoutOfData();


        Vbo vboColor;
        vboColor.createVbo(1);
        vboColor.setBasicDescription(1, GL_ARRAY_BUFFER, GL_STATIC_DRAW, GL_FLOAT, sizeof(float), 3);
        vboColor.bindVbo();
        vboColor.fillVbo(colors, sizeof(float) *72 );
        vboColor.setLayoutOfData();

        Vbo indexBuffer;
        indexBuffer.createVbo(1);
        indexBuffer.setBasicDescription(-1,GL_ELEMENT_ARRAY_BUFFER, GL_STATIC_DRAW, GL_INT, sizeof(unsigned int), 6);
        indexBuffer.bindVbo();
        indexBuffer.fillVbo(index, sizeof(unsigned int) * 36);



        std::string vertexShader;
        readShaderFile(R"(E:\Workspace\lenoProjects\c++_gameEngine\LngEngine\resources\shaders\test_basic\basicVert.vert)", &vertexShader);
        std::string fragmentShader;
        readShaderFile(R"(E:\Workspace\lenoProjects\c++_gameEngine\LngEngine\resources\shaders\test_basic\basicFrag.frag)", &fragmentShader);


        Mat4x4<float> matFloat;
    	
        Mat4x4<float> matFloatX;
        Mat4x4<float> matFloatY;
        Mat4x4<float> matFloatZ;
        Mat4x4<float> perspective;
        Mat4x4<float> translate;
        Mat4x4<float> rotation;


        glm::mat4 Projection = glm::perspective(glm::pi<float>() * 0.25f, 4.0f / 3.0f, 0.1f, 1200.f);

        setPerspectiveMatrix(&perspective, glm::pi<float>() * 0.25f, 0.01f, 1200.f, (4.f / 3.f));
        setIdentity(&translate);




        std::cout << glm::to_string(Projection) << std::endl;;

        perspective.print();
    	

        setIdentity(&matFloat);
        setIdentity(&matFloatX);
        setIdentity(&matFloatY);
        setIdentity(&matFloatZ);
        setIdentity(&rotation);

    	

        unsigned int shader = CreateShader(vertexShader, fragmentShader);

        glUseProgram(shader);

        float test[16];


        float angle = 0.0f;


        glm::mat4 projection = glm::perspective(glm::radians(75.f), 4.f / 3.f, 0.1f, 1000.f);
    	

    	int uniformLocationWorld= glGetUniformLocation(shader, "worldMatrix");
    	int uniformRotation= glGetUniformLocation(shader, "rotation");
    	

        std::cout << vertexShader << std::endl;
        


        vao.activateVao();
        glEnableVertexAttribArray(0);
        glEnableVertexAttribArray(1);

        float a = -1;
        float angleB = 0;

        float z = 0.2f;

        std::cout << std::cos(1.57079633) << std::endl;
        glm::mat4 myMatrix = glm::translate(glm::mat4(1), glm::vec3(0.1f, 0.0f, 0.0f));

        ModelMatrix model;

        model.setPerspectiveMatrix(glm::pi<float>() * 0.25f, 0.1f, 1000.f, 4.f / 3.f);

        ModelVertex modelVertex;

        float zMove = 0;
        zMove = 0.0001 ;
        float fullCircle = (glm::pi<float>() * 2);
        float nineDeggree = (glm::pi<float>() * 2)/ 4;
        int ab;
        glEnable(GL_DEPTH_TEST);
        std::clock_t start;
        double duration;

        start = std::clock();
        long desiredFramePerSeconds =1000 / 60 ;
        long oneSecondTimer = 0.0f;
        long end;
        int frameCounter = 0;;
		while (!glfwWindowShouldClose(window)) {
            start = std::clock();
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

            glDrawElements(GL_TRIANGLES,36,GL_UNSIGNED_INT,0);

            glfwSwapBuffers(window);
            glfwPollEvents();
   
            zMove += 0.00001;
            
      

            glUniformMatrix4fv(uniformLocationWorld, 1, GL_FALSE, &model.getComputedMatrix(&modelVertex)[0][0]);
       
            glUniformMatrix4fv(uniformRotation, 1, GL_FALSE, &model.getRotation()[0][0]);
   
  
            end = std::clock();
            long difference = end - start;
			if(difference < desiredFramePerSeconds)
			{
				do
				{
                    difference = std::clock() - start;
                } while (difference < desiredFramePerSeconds);
                
			}

            ++frameCounter;
			if(std::clock() - oneSecondTimer >= 1000)
			{
              
				frameCounter = 0;
                frameCounter = 0;
                oneSecondTimer = std::clock();
			}
			

        }


    	
    }
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
