#pragma once
#include "development_def.h"


struct VaoDescriptor {
    unsigned int id;
};

struct VboDescriptor {
    unsigned int id;
    unsigned int target;
    unsigned int dataType;
    unsigned int typeOfUsage;
    unsigned int attrIndex;
    unsigned int sizeOfData;
    unsigned int sizeOfVertex;
    unsigned int sizeOfDataType;

};
