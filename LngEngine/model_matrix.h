#pragma once
#include "glm.hpp"
#include "gtc/matrix_transform.hpp"
#include "gtx/string_cast.hpp"
#include "model_vertex.h"
#include "gtx/quaternion.hpp"
#include "gtc/quaternion.hpp"


class ModelMatrix
{
public:
	ModelMatrix();
	~ModelMatrix();
	glm::mat4 perspective;
	
	void setPerspectiveMatrix(float fov, float near, float far, float aspect);
	
	glm::mat4 getComputedMatrix(ModelVertex* modelVertex);

	float angle = 0;
	glm::mat4 getRotation();
	glm::mat4 getRotationConjugate();
	

	void printMatrices();
private:

protected:
};

